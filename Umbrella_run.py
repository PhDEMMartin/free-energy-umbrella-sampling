from time import monotonic
from subprocess import run
import os
import shutil


def get_time():
    time = monotonic()
    return time


def reader(file):

    frames = []

    with open(file, 'r+') as handle:
        for i in handle:
            if 'frame' in i:
                pass
            else:
                frame = i.strip()
                frame = frame.split(' ')
                frame = frame[0]
                frames.append(frame)

    return frames


def simulations(file):
    start = get_time()

    frames = reader(file)

    first_frame = 0

    if os.path.exists('sim_log.txt'):
        sim_list = []
        with open('sim_log.txt', 'r') as handle_1:
            for last in handle_1:
                sim_list.append(last)
            last_sim = sim_list[-1]
            counter = 0
            for k in frames:
                counter += 1
                if k == last_sim:
                    first_frame = counter
                    break

    with open('sim_log.txt', 'w+') as handle:
        shutil.copyfile('%s/gromacs_run.sh' % parental_directory, '%s/gromacs_run.sh' % change)
        os.chmod('%s/gromacs_run.sh' % change, 0o755)
        # run('%s/get_distances.sh' % path_to)
        os.system('./gromacs_run.sh')
        if os.path.exists('%s/gromacs_run.sh' % change):
            os.remove('%s/gromacs_run.sh' % change)
        # run('%s/gromacs_run.sh' % parental_directory)
        # os.system('source /usr/local/gromacs/bin/GMXRC')
        # os.system('gmx --version')
        for frame in range(first_frame, len(frames)):
            current_time = get_time()
            current_frame = int(frames[frame])
            if current_time >= start + 28800:
                os.system("shutdown now -h")
            else:
                out_file = "frame-%i_run-umbrella.sh" % current_frame
                if os.path.exists(out_file):
                    os.chmod(out_file, 0o755)
                    os.system("./%s" % out_file)
                    handle.write(str(current_frame))

                else:
                    print("No such file exist\n %s" % out_file)

    return frames


def umbrella(frames):
    counter = 0
    missing = []
    with open('tpr_files.dat', 'w') as handle_2:
        for window in frames:
            archive = 'umbrella%s.tpr' % window
            if os.path.exists(archive):
                handle_2.write('%s\n' % archive)
                counter += 1

            else:
                missing.append(window)
        handle_2.close()

    with open('pullf_files.dat', 'w') as handle_3:
        for window_1 in frames:
            archive_1 = 'umbrella%s_pullf.xvg' % window_1
            if os.path.exists(archive_1):
                handle_3.write('%s\n' % archive_1)
                counter += 1
        handle_2.close()

    if counter != len(frames):
        print('these are the missing frames:', missing)
    else:
        shutil.copyfile('%s/wham_run.sh' % parental_directory, '%s/wham_run.sh' % change)
        os.chmod('%s/wham_run.sh' % change, 0o755)
        os.system('./wham_run.sh')
        if os.path.exists('%s/wham_run.sh' % change):
            os.remove('%s/wham_run.sh' % change)


def main(file):
    frames = simulations(file)
    umbrella(frames)


if __name__ == "__main__":
    parental_directory = os.getcwd()
    file_1 = 'FE_US_TEST'
    path_1 = '/home/pc-gamer/Escritorio/MD_sim'
    change = '%s/%s/' % (path_1, file_1)
    os.chdir(change)
    file = '%sframes_log.txt' % change
    main(file)



