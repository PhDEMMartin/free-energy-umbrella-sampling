import os
import re
from Bio.PDB import *


def files():
    gro_files = []
    pdb_files = []
    for filename in os.listdir(parental_directory):
        if filename.endswith('gro'):
            gro_files.append(filename)

        elif filename.endswith('pdb'):
            pdb_files.append(filename)

    for gro_file in gro_files:
        for pdb_file in pdb_files:
            try:
                if gro_file.split('.')[0] == pdb_file.split('.')[0]:
                    return gro_file, pdb_file

            except Exception as e_files:
                print(f'No PDB file has been found yet: {e_files}')

    return [], []


def start_end(gro, pdb, complete_path=''):
    name = pdb.split('.')[0]
    parser = PDBParser(QUIET=True)
    chains_dict = {}

    if os.path.exists(f'{complete_path}{pdb}'):
        try:
            model = parser.get_structure(id=name, file=f'{complete_path}{pdb}')
            chains = Selection.unfold_entities(model, 'C')  # Extract chains
            chains = [chain.id for chain in chains]

            for subunit in chains:
                residues = list(model[0][subunit].get_residues())
                residue_start = [residues[0].get_resname(), str(residues[0].get_id()[1])]
                residue_end = [residues[-1].get_resname(), str(residues[-1].get_id()[1])]

                # Initialize chains_dict with residues and placeholders for atom info
                chains_dict[subunit] = {
                    "start": {"residue": residue_start, "atoms": [], "last_gro_num": 0},  # Track last GRO number
                    "end": {"residue": residue_end, "atoms": [], "last_gro_num": 0},  # Track last GRO number
                }
        except Exception as e_pdb:
            print(f'The PDB could not be retrieved: {e_pdb}')

    if os.path.exists(f'{complete_path}{gro}'):
        try:
            with open(f'{complete_path}{gro}', 'r') as atoms:
                for line in atoms:
                    try:
                        # Parse the GRO file line
                        match = re.match(r'(\d+)([A-Z]{3})$', line.split()[0])  # Match residue info
                        if match:
                            position = match.group(1)
                            res_name = match.group(2)
                            atom_name = line.split()[1]  # Atom name
                            gro_numeration = int(line.split()[2])  # Atom index in GRO file

                            for subunit in chains_dict:
                                info = chains_dict[subunit]

                                # Match start residue, ensuring no duplication using `last_gro_num`
                                if (
                                    res_name == info["start"]["residue"][0]
                                    and position == info["start"]["residue"][1]
                                    and gro_numeration > info["start"]["last_gro_num"]  # Ensure no overlap
                                ):
                                    info["start"]["atoms"].append({"name": atom_name, "gro_num": gro_numeration})
                                    info["start"]["last_gro_num"] = gro_numeration  # Update last GRO atom number

                                # Match end residue, ensuring no duplication using `last_gro_num`
                                if (
                                    res_name == info["end"]["residue"][0]
                                    and position == info["end"]["residue"][1]
                                    and gro_numeration > info["end"]["last_gro_num"]  # Ensure no overlap
                                ):
                                    info["end"]["atoms"].append({"name": atom_name, "gro_num": gro_numeration})
                                    info["end"]["last_gro_num"] = gro_numeration  # Update last GRO atom number
                    except Exception as e_gro_line:
                        print(f'Gro file line parsing failed: {e_gro_line}')
        except Exception as e_gro:
            print(f'The GROMACS file could not be retrieved: {e_gro}')
    return chains_dict


def atom_number(a, b, c, d):

    residues = [a, b, c, d]

    residue_atoms = {}
    for each in residues:
        residue_atoms[each] = None

    with (open('npt.gro', 'r') as handle_it):
        for line in handle_it:
            for residue in residues:
                if residue in line:
                    line = line.strip()
                    line = line.split(' ')
                    counter = 0
                    delete = []
                    for element in line:
                        counter += 1
                        if element == '':
                            delete.append(counter-1)
                    delete.reverse()
                    for number in delete:
                        line.pop(number)
                    atoms_list = residue_atoms.get(line[0])
                    if atoms_list is None:
                        residue_atoms[line[0]] = line[2]
                    else:
                        if type(atoms_list) is str:
                            first = atoms_list
                            atoms_list = [first, line[2]]
                            residue_atoms[line[0]] = atoms_list
                        else:
                            atoms_list.append(line[2])
                            residue_atoms[line[0]] = atoms_list

    a_start = residue_atoms.get(a)[0]
    a_end = residue_atoms.get(b)[-1]
    b_start = residue_atoms.get(c)[0]
    b_end = residue_atoms.get(d)[-1]

    return a_start, a_end, b_start, b_end


# def index(pointers):
#
#     chains_indx = {}
#
#     with (open('index.ndx', 'r') as handle):
#         tag = False
#         atoms = []
#         chain = list(pointers.keys())[0]
#         chains_all_atoms = []
#
#         for line in handle:
#             subunit_matcher = re.match(r'^(\[ Chain_)([A-Z]+)_(\d+)( ])$', line)
#             if subunit_matcher:
#                 tag = True
#                 subunit_start = pointers[str(subunit_matcher.group(2))]['start']['atoms'][0]['gro_num']
#                 subunit_end = pointers[str(subunit_matcher.group(2))]['end']['atoms'][-1]['gro_num']
#                 current_chain = str(subunit_matcher.group(2))
#             if (
#                     tag
#                     and subunit_matcher is None
#                     and chain == current_chain
#             ):
#                 line = line.strip()
#                 line = re.split(r'\s+', line)
#                 for each in line:
#                     atom_num_match = re.fullmatch(r'(\d+)', each)
#                     if atom_num_match:
#                         atoms.append(each)
#                 for atom in atoms:
#                     if subunit_start <= int(atom) <= subunit_end:
#                         chains_all_atoms.append(atom)
#                 atoms = []
#             elif tag and subunit_matcher is None and chain != current_chain:
#                 chains_indx[chain] = chains_all_atoms
#                 chain = current_chain
#                 chains_all_atoms = []
#                 line = line.strip()
#                 line = re.split(r'\s+', line)
#                 for each in line:
#                     atom_num_match = re.fullmatch(r'(\d+)', each)
#                     if atom_num_match:
#                         atoms.append(each)
#                 for atom in atoms:
#                     if subunit_start <= int(atom) <= subunit_end:
#                         chains_all_atoms.append(atom)
#                 atoms = []
#
#         chains_indx[chain] = chains_all_atoms
#         chains_all_atoms = []
#         handle.close()
#
#     with (open('index.ndx', 'r') as handle_1):
#         with (open('index2.ndx', 'w+') as handle_2):
#             for line_1 in handle_1:
#                 match_1 = re.match(r'^(\[ Chain_)([A-Z]+)_(\d+)( ])$', line_1)
#                 if match_1:
#                     handle_1.close()
#                     break
#                 handle_2.write(line_1)
#             for subunit in chains_indx:
#                 handle_2.write(f'[ Chain_{subunit} ]\n')
#                 set_length = len(chains_indx[subunit])
#                 set_start = 0
#                 atoms_list = []
#                 for atoms_set in range(15, set_length, 15):
#                     for atom in range(set_start, atoms_set):
#                         dig_num = len(re.findall(r'\d', chains_indx[subunit][atom]))
#                         if dig_num == 1:
#                             atoms_list.append(f'{(" " * 4)}{chains_indx[subunit][atom]}')
#                         elif dig_num == 2:
#                             atoms_list.append(f'{(" " * 3)}{chains_indx[subunit][atom]}')
#                         elif dig_num == 3:
#                             atoms_list.append(f'{(" " * 2)}{chains_indx[subunit][atom]}')
#                         elif dig_num > 3:
#                             atoms_list.append(f'{(" " * 1)}{chains_indx[subunit][atom]}')
#                     start_spaces = len(re.findall(r'\s',atoms_list[0]))
#                     number = re.search(r'(\d)+$', atoms_list[0])
#                     atoms_list[0] = f'{(" " * int(start_spaces))}{number.group(0)}'
#                     line = ''.join(atoms_list) + '\n'
#                     handle_2.write(line)
#                     atoms_list = []
#                     set_start = atoms_set
#         handle_2.close()


def index(pointers):
    chains_indx = {}
    chain = None
    chains_all_atoms = []

    # Reading Phase
    with open('index.ndx', 'r') as handle:
        tag = False

        for line in handle:
            # Check for chain header line
            subunit_matcher = re.match(r'^(\[ Chain_)([A-Z]+)_(\d+)( ])$', line.strip())
            if subunit_matcher:
                # If we were processing a chain, save the collected atoms
                if chain is not None:
                    chains_indx[chain] = chains_all_atoms
                    chains_all_atoms = []
                # Start processing new chain
                chain = subunit_matcher.group(1)
                tag = True
                subunit_start = int(pointers[chain]['start']['atoms'][0]['gro_num'])
                subunit_end = int(pointers[chain]['end']['atoms'][-1]['gro_num'])
            elif tag:
                # Process line to collect atoms
                atoms_in_line = line.strip().split()
                for each in atoms_in_line:
                    if each.isdigit():
                        atom_num = int(each)
                        if subunit_start <= atom_num <= subunit_end:
                            chains_all_atoms.append(each)
        # After finishing the file, make sure to save the last chain
        if chain is not None:
            chains_indx[chain] = chains_all_atoms

    # Writing Phase
    with open('index.ndx', 'r') as handle_1, open('index2.ndx', 'w') as handle_2:
        # Copy lines until the first chain header
        for line_1 in handle_1:
            match_1 = re.match(r'^(\[ Chain_)([A-Z]+)_(\d+)( ])$', line_1.strip())
            if match_1:
                # Once we reach the chain headers, stop copying
                break
            handle_2.write(line_1)
        # Now write chains from 'chains_indx'
        for subunit in chains_indx:
            handle_2.write(f'[ Chain_{subunit} ]\n')
            total_atoms = chains_indx[subunit]
            set_length = len(total_atoms)

            # Process atoms in chunks of 15 using a for loop
            for set_start in range(0, set_length, 15):
                atoms_list = []
                atoms_set = min(set_start + 15, set_length)
                for atom_index in range(set_start, atoms_set):
                    atom = total_atoms[atom_index]
                    dig_num = len(re.findall(r'\d', atom))
                    if dig_num == 1:
                        atoms_list.append(f'{" " * 4}{atom}')
                    elif dig_num == 2:
                        atoms_list.append(f'{" " * 3}{atom}')
                    elif dig_num == 3:
                        atoms_list.append(f'{" " * 2}{atom}')
                    else:
                        atoms_list.append(f'{" " * 1}{atom}')
                # Adjust spacing for the first atom in the line
                if atoms_list:
                    start_spaces = len(re.findall(r'\s', atoms_list[0]))
                    number = re.search(r'\d+$', atoms_list[0])
                    if number:
                        atoms_list[0] = f'{" " * start_spaces}{number.group(0)}'
                    line = ''.join(atoms_list) + '\n'
                    handle_2.write(line)


def main():
    gro_file, pdb_file = files()
    chains = start_end(gro_file, pdb_file)
    index(chains)


if __name__ == "__main__":

    parental_directory = os.getcwd()

    main()
